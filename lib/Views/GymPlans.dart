import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GymPlans extends StatelessWidget {
  const GymPlans({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Gym Plans"),
        backgroundColor: Colors.blueAccent,
      ),
      body: Column(
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.only(top:70),
              child: SizedBox(
                width: 320,
                height: 280,
                child: Image(
                  image: AssetImage("images/gymplans.jpg"),
                ),
              ),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.only(top:150),
              child: Container(
                color: Colors.black,
                height: 80,
                width: double.maxFinite,
                child: Center(
                  child: Text("Our plans start from R249 p/m",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                  ),),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Dashboard.dart';

class Registration extends StatelessWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text("Registration"),
      backgroundColor: Colors.blueAccent,
    ),

        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top:120),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextField(
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.brown,
                          fontSize: 15
                      ),
                      decoration: InputDecoration(
                          labelText: "Name",
                          icon: Icon(Icons.info),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:40),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextField(
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.brown,
                          fontSize: 15
                      ),
                      decoration: InputDecoration(
                          labelText: "Surname",
                          icon: Icon(Icons.info_outline_rounded),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:40),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextField(
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.brown,
                          fontSize: 15
                      ),
                      decoration: InputDecoration(
                          labelText: "Email",
                          icon: Icon(Icons.lock_clock),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:40),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextField(
                      obscureText: true,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.brown,
                        fontSize: 15,


                      ),
                      decoration: InputDecoration(
                          labelText: "Password",
                          icon: Icon(Icons.lock),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:40),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextField(
                      obscureText: true,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.brown,
                        fontSize: 15,


                      ),
                      decoration: InputDecoration(
                          labelText: "Confirm Password",
                          icon: Icon(Icons.lock),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),
              //add a button here
              Padding(
                padding: const EdgeInsets.only(top: 60),
                child: TextButton(
                  onPressed: (){
                    Navigator.push(context,MaterialPageRoute(
                        builder: (context){
                      return Dashboard();
                    }));
                  },
                  child: Container(
                    color: Colors.blueAccent,
                    width: 200,
                    height: 40,
                    child: Center(child: Text("Register",
                      style: TextStyle(
                          color: Colors.white
                      ),)),
                  ),
                ),
              )
            ],
          ),
        )
    );
  }
}
